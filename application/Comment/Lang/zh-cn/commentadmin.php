<?php
return array(
		"Name" => '姓名',
		"EMAIL" => '邮箱',
		"TITLE" => '留言标题',
		"CONTENT" => '留言内容',
		"TIME" => '留言时间',
		"AUDITED" => '已审核',
		"NOT_AUDITED" => "未审核",
		"AUDIT" => '审核',
		"CANCEL_AUDIT" => '取消审核'
);